/* Name: Zubair Khan
 * H983T574
 * hw01 
 */
 
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include "DayOfYear.hpp"

DayOfYear::DayOfYear(int month_par, int day_par) 
	:month(month_par), day(day_par)
{
	//will check the date after initializing the values
	testDate();
}

DayOfYear::DayOfYear(int month_par)
	:month(month_par), day(1)
{
	testDate();
}

DayOfYear::DayOfYear()
	:month(1), day(1)
{ }

const DayOfYear operator +(const DayOfYear& dateOne, const DayOfYear& dateTwo)
{
	int sumMonth = dateOne.month + dateTwo.month;
	int sumDay = dateOne.day + dateTwo.day;
	
	//takes care of rollover in terms of days (30 day calendar)
	if (sumDay > 30)
	{
		sumMonth += sumDay/30;
		sumDay = sumDay%30;
	}
	
	//takes care of rollover for months
	if (sumMonth > 12)
	{
		sumMonth -= 12;
	}
	
	return DayOfYear(sumMonth, sumDay);
}

const DayOfYear DayOfYear::operator +(int addDays)
{
	int monthTmp = month, dayTmp = day;
	dayTmp += addDays;
	
	//for rollover
	if (dayTmp > 30)
	{
		monthTmp += dayTmp/30;
		dayTmp = dayTmp%30;
	}
	
	if (monthTmp > 12)
		monthTmp -= 12;
		
	return DayOfYear(monthTmp, dayTmp); 
}

//used if user tries to do int+obj
const DayOfYear operator +(int parameter, const DayOfYear& dateOne)
{
	int monthTmp = dateOne.getMonthNumber();
	int dayTmp = dateOne.getDay();
	dayTmp += parameter;
	
	//for rollover
	if (dayTmp > 30)
	{
		monthTmp += dayTmp/30;
		dayTmp = dayTmp%30;
	}
	
	if (monthTmp > 12)
		monthTmp -= 12;
		
	return DayOfYear(monthTmp, dayTmp); 
}

const int operator -(const DayOfYear& dateOne, const DayOfYear& dateTwo)
{
	int dateOneDays = (30 * dateOne.getMonthNumber()) + dateOne.getDay();
	int dateTwoDays = (30 * dateTwo.getMonthNumber()) + dateTwo.getDay();
	
	//won't do final computation if r-value is greater than l-value
	if (dateOneDays < dateTwoDays)
	{
		std::cout << "Can't subtract these values.\n" 
		<< "R-value is larger than L-value\n";
		
		return 0;
	}
	
	int difference = dateOneDays - dateTwoDays;
	
	return difference;
}
 
const DayOfYear DayOfYear::operator -()const
{
	int dateOneDays = ((30 * month) + day);
	
	//takes away from 361 because have to include 1/1 - needs extra day
	int fromEndOfYear = 361 - dateOneDays;
	
	//using remaining days to get to the date from the end of the year
	int monthsLeft = fromEndOfYear/30;
	int daysLeft = fromEndOfYear%30;
	
	return DayOfYear(monthsLeft, daysLeft);
}

bool DayOfYear::operator ==(const DayOfYear& dateOne) const
{
	int dateOneDays = (30 * dateOne.getMonthNumber()) + dateOne.getDay();
	
	//checks if the int values of total days are equal
	if (dateOneDays == ((month*30)+day))
		return true;
	else
		return false;
}

bool DayOfYear::operator <(const DayOfYear& dateTwo)
{
	int dateTwoDays = (30 * dateTwo.getMonthNumber()) + dateTwo.getDay();
	
	if (((month*30)+day) < dateTwoDays)
		return true;
	else
		return false;
}

bool DayOfYear::operator >(const DayOfYear& dateTwo)
{
	int dateTwoDays = (30 * dateTwo.getMonthNumber()) + dateTwo.getDay();
	
	if (((month*30)+day) > dateTwoDays)
		return true;
	else
		return false;
}

DayOfYear& DayOfYear::operator ++()
{
	++day;
	
	//adjusts for roll over if the incrementing goes to 31
	if (day > 30)
	{
		month += day/30;
		day -= 30;
	}
	
	if (month > 12)
		month -= 12;
	
	return *this;
}

const DayOfYear DayOfYear::operator ++(int)
{
	int tmpDay = day;
	
	++*this;
	
	if (day > 30)
	{
		month += day/30;
		day -= 30;
	}
	
	if (month > 12)
		month -= 12;
	
	return DayOfYear(month, tmpDay);
}

DayOfYear& DayOfYear::operator --()
{
	--day;
	
	//goes back a month if day becomes 0
	if (day < 1)
	{
		month -= 1;
		day = 30;
	}
	
	//will take care of decrementing from 1/1
	if (month == 0)
		month = 12;
	
	return *this;
}

const DayOfYear DayOfYear::operator --(int)
{
	int tmpDay = day;
	
	--*this;
	
	if (day < 1)
	{
		month -= 1;
		day = 30;
	}
	
	if (month == 0)
		month = 12;
	
	return DayOfYear(month, tmpDay);
}

std::istream& operator >>(std::istream& inStream, DayOfYear& dateObject)
{
	std::string strDate;
	inStream >> strDate;
	
	DayOfYear defaultDate(1,1);
	
	int strLength = strDate.length();
	
	if (strLength < 3 || strLength > 5)
	{
		std::cout << "Invalid input for string\n";
		
		dateObject = defaultDate;
	}
	
	int slashIndex = strDate.find_first_of('/');
	
	if (slashIndex != 1 && slashIndex != 2)
	{
		std::cout << "Invalid input for string\n";
		
		dateObject = defaultDate;
	}		
	
	if (strLength == 3)
	{
		if (slashIndex != 1)
		{
			std::cout << "Invalid input for string\n";
			
			dateObject = defaultDate;
		}
		
		else{
		int start, end;
		
		start = std::stoi(strDate);
		std::string hold;
		hold.insert(0, 1, strDate[2]);
		end = std::stoi(hold);
		
		dateObject.month = start;
		dateObject.day = end;
		
		dateObject.testDate();
		}
	}

	if (strLength == 4)
	{
		int start(0), end(0);
		
		if (slashIndex == 1)
		{
			start = std::stoi(strDate);
			std::string hold;
			hold.insert(0, 1, strDate[2]);
			hold.insert(1, 1, strDate[3]);
			end = std::stoi(hold);
		}

		else if (slashIndex == 2)
		{
			start = std::stoi(strDate);
			std::string hold;
			hold.insert(0, 1, strDate[3]);
			end = std::stoi(hold);
		}
		
		dateObject.month = start;
		dateObject.day = end;
		
		dateObject.testDate();
	}
		
	if (strLength == 5)
	{
		if (slashIndex != 2)
		{
			std::cout << "Invalid input for string\n";
			dateObject = defaultDate;
		}
		
		else {		
		int start, end;
		
		start = std::stoi(strDate);
		std::string hold;
		hold.insert(0, 1, strDate[3]);
		hold.insert(1, 1, strDate[4]);
		end = std::stoi(hold);
		
		dateObject.month = start;
		dateObject.day = end;
		
		dateObject.testDate();
		}
	}
	
	//takes care of the carriage return/space at the end of each input
	std::cin.ignore();
	
	return inStream;
}

std::ostream& operator <<(std::ostream& outStream, const DayOfYear& date)
{
    switch (date.month) {
    case 1:
        outStream << "January ";
        break;
    case 2:
        outStream << "February ";
        break;
    case 3:
        outStream << "March ";
        break;
    case 4:
        outStream << "April ";
        break;
    case 5:
        outStream << "May ";
        break;
    case 6:
        outStream << "June ";
        break;
    case 7:
        outStream << "July ";
        break;
    case 8:
        outStream << "August ";
        break;
    case 9:
        outStream << "September ";
        break;
    case 10:
        outStream << "October ";
        break;
    case 11:
        outStream << "November ";
        break;
    case 12:
        outStream << "December ";
        break;
    default:
        outStream << "Error in DayOfYear::output.";
    }
    outStream << date.day;
    
    return (outStream);
}

int DayOfYear::operator [](int index) const
{
	if (index == 1) 
		return month;
	
	else if (index == 2) 
		return day;
	
	else if (index==3)
	{
		//take away one month so January 1st is 1 instead of 31
		int totalDays = (30*(month-1)) + day;
		return totalDays;
	}
	
	else
		return -1;	
}

int DayOfYear::getMonthNumber() const
{
	return month;
}

int DayOfYear::getDay() const
{
	return day;
}

void DayOfYear::testDate()
{
    if (month < 1 || month > 12) {
        std::cout << "Illegal month value or operation!" << std::endl 
        << "Resetting month...\n" << std::endl;
		month = 1;
    }
    if (day < 1 || day > 30) {
        std::cout << "Illegal day value or operation!" << std::endl 
        << "Resetting day...\n" << std::endl;
        day = 1;
    }
}

